# Submission Notes

These notes will be read by HubSpot developers. Drop us a line!

## Given more time, what would you have done differently?

I would have looked into filtering the year and genre options so that only options with available items would be displayed.

## How did you deviate from the directions, if at all, and why?

I didn't use the original repo as I decided to build the page using Vue.js. I have copied over whatever was necessary for the tasks. For the first task, I kept the structure as is but I did update it with more semantic elements for better accessibility.

## Is there anything else you'd like to let us know?

I've done my best to hit any bonus points on thes tasks, using various npm packages to do so. I'd be happy to explain more on these if needed (i.e fuse.js for fuzzy search).

I have put a demo of the project online at https://reverent-noether-fce2e5.netlify.app/