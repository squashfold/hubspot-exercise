import { createStore } from "vuex";

export default createStore({
  state: {
    movies: [],
    searchResults: [],
    genres: [],
    years: [],
  },
  getters: {
    getMovies: (state) => {
      return state.movies;
    },
    getSearchResults: (state) => {
      return state.searchResults;
    },
    getGenres: (state) => {
      return state.genres;
    },
    getYears: (state) => {
      return state.years;
    },
  },
  mutations: {
    SET_MOVIES(state, data) {
      state.movies = data;
    },
    SET_SEARCHRESULTS(state, data) {
      state.searchResults = data;
    },
    SET_GENRES(state, data) {
      state.genres.push(data);
    },
    SET_YEARS(state, data) {
      state.years.push(data);
    },
  },
  actions: {
    setMovies({ commit }, newValue) {
      commit("SET_MOVIES", newValue);
    },
    setSearchResults({ commit }, newValue) {
      commit("SET_SEARCHRESULTS", newValue);
    },
    addGenre({ commit }, newValue) {
      commit("SET_GENRES", newValue);
    },
    addYear({ commit }, newValue) {
      commit("SET_YEARS", newValue);
    },
  },
  modules: {},
});
